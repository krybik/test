<?php
declare(strict_types=1);


class ConnectionData
{
    // имя бд
private static $DBName = 'employee';
// имя
private static $user = 'root';
private static $masterUser = 'root';
private static $password = '';
private static $masterPassword = '';
private static $dsn = 'mysql:host=localhost;dbname=';
private static $dsn_for_create = 'mysql:host=localhost';

    /**
     * @return string
     */
    public static function getMasterPassword(): string
    {
        return self::$masterPassword;
    }
    /**
     * @return string
     */
    public static function getMasterUser(): string
    {
        return self::$masterUser;
    }
    /**
     * @return string
     */
    public static function getDsnForCreate(): string
    {
        return self::$dsn_for_create;
    }
    /**
     * @return string
     */
    public static function getUser()
    {
        return self::$user;
    }

    /**
     * @return string
     */
    public static function getDBName()
    {
        return self::$DBName;
    }

    /**
     * @return string
     */
    public static function getPassword()
    {
        return self::$password;
    }

    /**
     * @return string
     */
    public static function getDsn(): string
    {
        return self::$dsn;
    }
}