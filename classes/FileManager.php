<?php
declare(strict_types=1);

class FileManager
{
    private $PHOTO_DIR = '/photo/';
    private $ROOT;
    private $types_of_file = array(
        'image/png' => '.png',
        'image/jpeg' => '.jpg'
    );

    public function __construct()
    {
        $this->ROOT = $_SERVER['DOCUMENT_ROOT'];
    }

    public function deleteFile(string $file_name)
    {
        $file_name = $_SERVER['DOCUMENT_ROOT'] . $file_name;
        try {
            unlink($file_name);
        } catch (Error $e) {
// log
        }
    }

    public function saveFile(string $f_name = '')
    {
        if('' !== $f_name){
            return file_exists($f_name)? $f_name : null;
        }
        if (empty($_FILES)) {
            return null;
        }
        foreach ($_FILES as $file) {
            $file_name = md5($file['name']);
            $ext = $file['type'];
            if (array_key_exists($ext, $this->types_of_file)) {
                $file_name .= $this->types_of_file[$ext];
            } else {
                return false;
            }
            $full_name = $this->PHOTO_DIR . $file_name;
            if (!file_exists($this->ROOT)) {
                mkdir($this->ROOT);
            }
            move_uploaded_file($file["tmp_name"], $this->ROOT . $full_name);
        }
            return $full_name ?? false;
    }


}