<?php
declare(strict_types=1);


require_once 'ConnectionData.php';
require 'SQLQuery.php';

class DBManager
{
    private $dbh;

    public function __construct()
    {
        try {
            $this->dbh = new PDO(ConnectionData::getDsn() . ConnectionData::getDBName(), ConnectionData::getUser(), ConnectionData::getPassword());
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            $this->dbh = $this->createDB();
        }
    }

    public function addEmployee($photo, $name, $gender, $date)
    {
        try {
            $stmt = $this->dbh->prepare(SQLQuery::INSRT);
            $stmt->bindValue(1, $photo);
            $stmt->bindValue(2, $name);
            $stmt->bindValue(3, $gender);
            $stmt->bindValue(4, $date);
            $stmt->execute();
        } catch (PDOException $e) {
            if ('23000' === $e->getCode()) {
                die(json_encode(array('error' => 'Такой сотрудник уже существует!')));
            }
            if ('42S02' === $e->getCode()) {
               $this->dbh->exec(SQLQuery::CRT_EMPL_TBL);
            }
            die(json_encode(array('error' => $e->getCode())));

        }
    }

//  возвращает путь к фото сотрудника
    public function selectPhotoById(string $id)
    {
        $id = $this->dbh->quote($id);
        $sql = SQLQuery::SLCT_BY_ID . $id;
        return $this->query($sql)[0]['photo'];
    }

    public function selectEmployeeById(string $id)
    {
        $id = $this->dbh->quote($id);
        $sql = SQLQuery::SLCT_BY_ID . $id;
        return $this->query($sql)[0];
    }

    public function deleteEmployee(string $id)
    {
        $id = $this->dbh->quote($id);
        $sql = SQLQuery::DEL_EMPL . $id;
        return $this->dbh->exec($sql);
    }

    public function editEmployee($photo, $name, $gender, $date, $id)
    {
        try {
            $stmt = $this->dbh->prepare(SQLQuery::UPD);
            $stmt->bindValue(1, $photo);
            $stmt->bindValue(2, $name);
            $stmt->bindValue(3, $gender);
            $stmt->bindValue(4, $date);
            $stmt->bindValue(5, $id);
            $stmt->execute();
        } catch (PDOException $e) {
            die(json_encode(array('error' => $e->getCode())));
        }
    }

// передоваемый запрос ДОЛЖЕН быть "безопасным"!!!
    public function query(string $sql)
    {
        try {
            $stmt = $this->dbh->query($sql);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            die(json_encode(array('error' => "Проверьте запрос")));
        }
    }

    public function getDBH()
    {
        return $this->dbh;
    }

    private function createDB()
    {
        try {
            $dbh = new PDO(ConnectionData::getDsnForCreate(),
                ConnectionData::getMasterUser(),
                ConnectionData::getMasterPassword());
            $dbh->exec("CREATE DATABASE `" . ConnectionData::getDBName() . "`;");

            $dbh = new PDO(ConnectionData::getDsn() . ConnectionData::getDBName(),
                ConnectionData::getUser(),
                ConnectionData::getPassword());
            $dbh->exec(SQLQuery::CRT_EMPL_TBL);

            return $dbh;
        } catch (PDOException $e) {
            die(json_encode(array('error' => $e->getMessage())));
//            die(json_encode(array('error' => "Ошибка! Обратитесь в службу поддержки!")));
        }
    }
}