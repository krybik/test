<?php
declare(strict_types=1);

/**
 * Построение запроса по поисковым критериям из формы
 */
class QueryBuilder
{
    private $DB;
    private $DBH;

    public function __construct(DBManager $DB)
    {
        $this->DB = $DB;
        $this->DBH = $DB->getDBH();
    }


    public function query(array $data)
    {
        //строим запрос
        $sql = $this->buildQuery($data);
        // выполняем запрос
        return $this->DB->query($sql);
    }

    private function buildQuery(array $data)
    {
        foreach ($data as $index => $value) {

        }
        $name = !empty($data['employee_name']) ? $this->DBH->quote('%' . $data['employee_name'] . '%') : null;
        $min = ('' !== $data['min']) ? intval($data['min']) : null;
        $max = ('' !== $data['max']) ? intval($data['max']) : null;
        if ($data['gender_m'] !== $data['gender_f']) {
            $gender = $this->DBH->quote(('true' === $data['gender_m']) ? 'm' : 'f');
        } else {
            $gender = null;
        }
        $end_query = array();
        if (!is_null($name)) {
            $end_query[] = '  name LIKE ' . $name;
        }
        if (!is_null($min)) {
            $end_query[] = SQLQuery::DATE . ' >= ' . $min;
//            $end_query[] = 'DATE_SUB(CURRENT_DATE, INTERVAL '.$min.' YEAR) >= birthday';
        }
        if (!is_null($max)) {
            $end_query[] = SQLQuery::DATE . ' <= ' . $max;
//            $end_query[] = 'DATE_SUB(CURRENT_DATE, INTERVAL '.$max.' YEAR) <= birthday';
        }
        if (!is_null($gender)) {
            $end_query[] = 'gender = ' . $gender;
        }
        $end_query = implode(' AND ', $end_query);
        $query = SQLQuery::SLCT_BASE . ' WHERE ' . $end_query;
        return $query;
    }

}