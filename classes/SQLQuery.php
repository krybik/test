<?php
declare(strict_types=1);

final class SQLQuery
{
    const CRT_EMPL_TBL = '
  CREATE TABLE IF NOT EXISTS employee(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  photo VARCHAR(50) NULL,
  name VARCHAR(255) NOT NULL,
  gender CHAR(1) NOT NULL,
   birthday DATE NOT NULL,
    UNIQUE (name, birthday)
  );
  ';
    const INSRT = "
  INSERT INTO employee ( photo, name, gender, birthday) VALUES (?,?,?,?);
  ";
    const SLCT_BASE = 'SELECT id, photo, name, gender, (DATE_FORMAT(FROM_DAYS(TO_DAYS(CURRENT_DATE) - TO_DAYS(birthday)), \'%Y\') + 0) AS age FROM employee';

    const DATE = '(DATE_FORMAT(FROM_DAYS(TO_DAYS(CURRENT_DATE) - TO_DAYS(birthday)), \'%Y\') + 0)';

    const SLCT_BY_ID = 'SELECT photo, name, gender, birthday FROM employee WHERE id = ';

    const DEL_EMPL = 'DELETE FROM employee WHERE id = ';

    const UPD = "UPDATE IGNORE employee SET  photo = ?, name = ?, gender = ?, birthday = ? WHERE id = ?;";
}