<?php
declare(strict_types=1);

class DataChecker
{
    public function check(array $data)
    {
        if ($this->checkData($data) && $this->checkParams($data)) {
            return true;
        }
        return false;
    }

    //проверка на запрещенные символы
    private function checkParams($data)
    {
        $pattern = '/(\W)/iu';
        foreach ($data as $value) {
            preg_match_all($pattern, $value, $matches);
            foreach ($matches[1] as $val) {
                if ('-' !== $val ) {
                    return false;
                }
            }
        }

        return true;

    }

    // проверка имен параметров по белому списку и отсутствия пустых значений
    private function checkData($data)
    {
        $white_list = array(
            'f_name',
            'l_name',
            'patronymic',
            'date',
            'gender'
        );
        if (5 !== count($data)) {
            return false;
        }
        foreach ($data as $key => $value) {
            if ('' === $value || !in_array($key, $white_list, true)) {
                return false;
            }
        }
        return true;
    }
}