<?php
declare(strict_types=1);

include 'classes/DBManager.php';
include 'list.php';
$f_names = explode(PHP_EOL, $first_names_f);
$m_names = explode(PHP_EOL, $first_names_m);
$m_l_names = explode(PHP_EOL, $m_last_names);
$f_l_names = explode(PHP_EOL, $f_last_names);
$f_pat = explode(PHP_EOL, $f_patro);
$m_pat = explode(PHP_EOL, $m_patro);


$m = array('0b52c47c7783ebbf1895a9f926aa933e.jpg','0e8a4b9fa5f8e6ef9518674474249cc9.jpg','80c75d2006fd68effe1dbf935061ca78.jpg','e9aedeaeaf4e82ffb9a563cda8f27589.jpg',);
$w = array('5c6e01d26eb0176d210b6312e1c6928e.jpg','af68845ac76ad21f2efd212bd0069e92.jpg','d64e169b4fa956e0cf935954e5671efd.jpg','e293980f5284065c57b0b86c6bce9816.jpg',);


function test(){

        echo randDate();
}
//test();

$DM = new DBManager();

for ($i =0; $i< 10000; $i++){
    generatePerson($DM);
}
echo 'all';

function generatePerson($db){
    global $f_names, $m_names, $m_l_names, $f_pat, $m_pat, $f_l_names, $m, $w;

$gender = mt_rand(0,1)? 'm':'f';
$file_name = 'm' === $gender ? randFromArray($m) : randFromArray($w);
$file_name = '/photo/'.$file_name;
$last_name = 'm' === $gender ? randFromArray($m_l_names) : randFromArray($f_l_names);
$first_name = 'm' === $gender ? randFromArray($m_names) : randFromArray($f_names);
$patro = 'm' === $gender ? randFromArray($m_pat) : randFromArray($f_pat);
$date = randDate();
$empl_name = $last_name.' '.$first_name.' '.$patro;
    $db->addEmployee($file_name, $empl_name, $gender, $date);
}


function randDate(){
    $y = mt_rand(1950,2015);
    $m = mt_rand(1,12);
    if (10 > $m){
        $m = '0'.$m;
    }
    $d = mt_rand(1,28);
    if (10 > $d){
        $d = '0'.$d;
    }
    return $y.'-'.$m.'-'.$d;
}


function randFromArray(array $arr){
    $key = mt_rand(0, count($arr) -1);
    return $arr[$key];

}