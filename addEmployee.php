<?php
declare(strict_types=1);

require_once 'classes/DBManager.php';

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $get = '/?edit=' . $id;
    $DB = new DBManager();
    $employee = $DB->selectEmployeeById($id);
    if (isset($employee)) {
        $person = explode(' ', $employee['name']);
        $l_name = $person[0] ?? '';
        $f_name = $person[1] ?? '';
        $l_name = $person[2] ?? '';
        if (!empty($employee['photo'])) {
            $get .= '&photo=' . $employee['photo'];
        }
        if ('m' === $employee['gender']) {
            $gender_m = true;
        } else {
            $gender_f = true;
        }
        $birthday = $employee['birthday'];
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Создание сотрудника</title>
    <link rel="stylesheet" href="/stylesheets/main.css">
    <script src="/js/jquery-3.2.0.min.js"></script>
</head>
<body>
<h1>Создание сотрудника</h1>
<div class="create_employee">
    <form id="cform" action="/createEmployee.php" method="POST" enctype="multipart/form-data">
        <div align="right">Фамилия: <input type="text" name="last_name" required
                                           value="<?php echo $person[0] ?? $person[0] ?>"></div>
        <div align="right">Имя: <input type="text" name="first_name" required
                                       value="<?php echo $person[1] ?? $person[1] ?>"></div>
        <div align="right">Отчество: <input type="text" name="patronymic" required
                                            value="<?php echo $person[2] ?? $person[2] ?>"></div>
        <div align="right">Дата рождения: <input type="date" name="date_of_birth" required
                                                 value="<?php echo $birthday ?? $birthday ?>"></div>
        <div align="right">Пол:
            <select name="gender" required>
                <option disabled selected value="">Выберите пол</option>
                <option value="m" <?php echo $gender_m ? 'selected' : '' ?>>Мужской</option>
                <option value="f" <?php echo $gender_f ? 'selected' : '' ?>>Женский</option>
            </select>
        </div>
        <div align="right">Фото: <input type="file" id="file" name="photo" value="<?php echo $photo ?? $photo ?>">
            <div id="status"></div>
        </div>
        <br>
        <div align="right">
            <button type="submit" id="cform_submit" name="cform_submit">Сохранить</button>
            <button id="cancel" onClick='location.href="/"'>Отмена</button>
            <input name="MAX_FILE_SIZE" value="204800" type="hidden"/>
        </div>

    </form>
</div>
</body>
<script>
    $(document).ready(function () {
        $('#cform').bind('submit', function (event) {
            event.preventDefault();
            var formData = new FormData();
            var can_send = true;
            $.each($("#file")[0].files, function (i, file) {
                    if (204800 < file.size) {
                        $('#status').html('Файл слишком большой!<br> Размер не должен превышать 200кб. ');
                        can_send = false;
                    }
                    formData.append('file-' + i, file);
                }
            );
            if (can_send) {
                formData.append('f_name', $("input[name='first_name']").val());
                formData.append('l_name', $("input[name='last_name']").val());
                formData.append('patronymic', $("input[name='patronymic']").val());
                formData.append('date', $("input[name='date_of_birth']").val());
                formData.append('gender', $("select").val());
                $.ajax({
                    url: '/createEmployee.php<?php if (isset($id)) echo $get?>',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    cache: false,
                    dataType: 'json',
                    data: formData,
                    timeout: 7000,
                    beforeSend: function (x) {
//                    console.log('beforeSend');
                    },//beforeSend
                    success: function (data) {
//                    console.log('success',data);
                        if (data !== undefined) {
                            if (undefined !== data.error) {
                                $('#status').html(data.error);
                            } else {
                                $('#status').html(data.status);
                                setTimeout(function () {
                                    location.href = "/";
                                }, 1000);
                            }
                        }//if
                        else {
                            console.log('success, but no data');
                        }//else
                    }
                });//ajax()
            }
        })
        //submit()

    })
    ;//.ready
</script>
</html>