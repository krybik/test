<?php
declare(strict_types=1);

require_once 'classes/DBManager.php';
require_once 'classes/QueryBuilder.php';

$p = $_POST;
//неверное количество параметров
if(5 !== count($p)){
    die();
}
$white_list = array(
    'employee_name',
    'min',
    'max',
    'gender_m',
    'gender_f'
);
// проверка имен параметров
foreach ($p as $key=>$value){
    if(!in_array($key, $white_list)){
        die();
    }
}
// недостаточно данных
if('' === $p['employee_name'] && '' === $p['min'] && '' === $p['min']){
    die(json_encode(array('error'=> 'Введите имя или возраст')));
}
// запрашиваем данные
$Query = new QueryBuilder(new DBManager());
$results =  $Query->query($p);




echo json_encode(array('employees'=>$results));