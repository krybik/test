<?php
declare(strict_types=1);

require_once 'classes/DBManager.php';
require_once 'classes/DataChecker.php';
require_once 'classes/FileManager.php';


// удаление сотрудника
if (isset($_GET['del'])) {
    $id = $_GET['del'];
    $DB = new DBManager();
    $photo = $DB->selectPhotoById($id);
    if (!is_null($photo)) {
        $FM = new FileManager();
        $FM->deleteFile($photo);
    }
    $ret = $DB->deleteEmployee($id);
    die(json_encode($ret));
}


// Добавление сотрудника

// контроль размера файла
$ContentLength = $_FILES['file-0']['size'] ??  0;
if (204800 < $ContentLength) {
    die(json_encode(array('error' => "Превышен размер фото!")));
}
$checker = new DataChecker();
if (!$checker->check($_POST)) {
    die(json_encode(array('error' => "Не заполнены все поля или<br> использованы недопустимые символы!")));
}

$FM = new FileManager();
if(!$file_name = $FM->saveFile()){
    die(json_encode(array('error' => "Неврный формат фото!")));
}

$DB = new DBManager();
$p = $_POST;
$empl_name = $p['l_name'] . ' ' . $p['f_name'] . ' ' . $p['patronymic'];

// редактировать запись
if (isset($_GET['edit'])) {
    // подставляем путь к фотографии
    if (is_null($file_name) && isset($_GET['photo'])) {
        $file_name = $FM->saveFile($_GET['photo']);
    }
    $DB->editEmployee($file_name, $empl_name, $p['gender'], $p['date'], $_GET['edit']);
} else {
// создать
    $DB->addEmployee($file_name, $empl_name, $p['gender'], $p['date']);
}
echo json_encode(array('status' => 'Успешно!'));


