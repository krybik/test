
var employees;
var view_on_page = 15;
var currentPage = 1;
var dt;


function viewResults(data) {
    dt = data;
// выводим сообщение с итогом поиска
    var msg = "Запрос выполнен<br>найдено " + data.length;
    $('#status').html(msg);
// подготавливаем массив строк таблицы
    employees = createRows(data);
// удаляем отображенные строки
    $('.employee').remove();
// отрисовываем новые
    $('#tb_employee').show();
// пагинатор
    $('#compact-pagination').pagination({
        items: employees.length,
        itemsOnPage: view_on_page,
        cssStyle: 'compact-theme',
        displayedPages: 3,
        edges: 1,
        prevText: 'Пред',
        nextText: 'След',
        useAnchors: false,
        onInit: function () {
            renderPage(currentPage);
        },
        onPageClick: function (a) {
            renderPage(a)
        }
    });

}


//----  ф-ии генерации строк таблицы с данными о сотрудниках    -----------

// принимает массив объектов с данными, возвращает массив HTML нод <tr>
function createRows(data) {
    var emplo = [];
    data.forEach(function (value, key) {
        emplo[key] = '' +
            '<tr class="employee">' +
            '<td>' + value['id'] + '</td>' +
            '<td>' + addPhoto(value['photo']) + '</td>' +
            '<td>' + value['name'] + '</td>' +
            '<td>' + value['age'] + '</td>' +
            changeGender(value['gender']) +
            '<td><a href="" id="edit">Ред</a>, <a href="" id="del">удал</a></td>' +
            '</tr>'
    });
    return emplo;
}
// вставить картинку если передан путь
function addPhoto(val) {
    return null !== val ? '<img class="photo" src="' + val + '">' : '';
}
function changeGender(gen) {
    return 'm' === gen ? '<td class="m">Муж</td>' : '<td class="f">Жен</td>';
}
//-------------------------------------------------------------------------


// отрисовка таблицы согласно номеру страницы ----------------------------
function renderPage(page_number) {
    currentPage = page_number;
//удаляем отрисованное
    $('.employee').remove();
// определяем индексы элементов для отрисовки
    var first = page_number * view_on_page - view_on_page;
    var last = page_number * view_on_page - 1;
// отрисовываем выбранные элементы
    for (var i = first; i <= last; i++) {
        if (undefined !== employees[i]) {
            $('tbody').append(employees[i]);
        }
    }
//  вешаем обработчик на кнопки действия
    $('#edit, #del').bind('click', changeEmployee());
//  увеличиваем фотки при наведении курсора
    $('.photo').hover(function (a) {
        var zoom = $('#zoom');
        zoom.append(a.currentTarget.outerHTML);
        zoom.children().first().addClass('zm');
    }, function () {
        $('.zm').remove();
    })
}
// обработчик действия над данными сотрудника
function changeEmployee() {
    return function (event) {
        event.preventDefault();
        event.stopPropagation();
        var id = event.currentTarget.parentNode.parentNode.firstChild.innerText;
        switch (event.currentTarget.id) {
            case 'edit':
                editEmployee(id);
                break;
            case 'del':
                deleteEmployee(id);
                break;
            default:
                console.log('error');
        }
    }
}

function editEmployee(id) {
    location.href = "/addEmployee.php/?id=" + id;
}

// удаление данных сотрудника
// применена ф-я confirm, желательно заменить на всплывающее окно
// запрос на удаление из БД
function deleteEmployee(id) {
    if (confirm('Вы пытаетесь удалить запись о сотруднике!\nПодтвердите!')) {
        var data = {del: id};
        $.ajax({
            url: '/createEmployee.php',
            type: 'GET',
            data: data,
            success: function (data) {
// если удалилен из базы, удалить из массива результатов и отрисовать новую таблицу
                if (data && '1' === data) {
                    dt.forEach(function (value, key) {
                        if (id === value.id) {
                            dt.splice(key, 1);
                            viewResults(dt);
                        }
                    });
                }
            }
        });
    }
}

function searchEmployee() {
    return function () {
        $('#status').html('Ищу');
        var formData = new FormData;
        formData.append('employee_name', $('input[name="employee_name"]').val());
        formData.append('gender_m', $('input[name="gender_m"]').prop('checked'));
        formData.append('gender_f', $('input[name="gender_f"]').prop('checked'));
        formData.append('min', $('input[name="min"]').val());
        formData.append('max', $('input[name="max"]').val());
        $.ajax({
            url: '/findEmployee.php',
            type: 'POST',
            contentType: false,
            processData: false,
            cache: false,
            dataType: 'json',
            data: formData,
            timeout: 7000,
            success: function (data) {
                if (data.error) {
                    $('#status').html(data.error);
                }
                if (data.employees) {
                    viewResults(data.employees);
                }
            },
            error: function (data) {
//                    console.log('error');
            }
        });
    }
}